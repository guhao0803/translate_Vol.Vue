/*****************************************************************************************
**  Author:jxx 2022
**  QQ:283591387
**完整文档见：http://v2.volcore.xyz/document/api 【代码生成页面ViewGrid】
**常用示例见：http://v2.volcore.xyz/document/vueDev
**后台操作见：http://v2.volcore.xyz/document/netCoreDev
*****************************************************************************************/
//此js文件是用来自定义扩展业务代码，可以扩展一些自定义页面或者重新配置生成的代码
import gridHeader from './Select_Assignee.vue'
let extension = {
  components: {
    //查询界面扩展组件
    gridHeader: gridHeader,
    gridBody: '',
    gridFooter: '',
    //新建、编辑弹出框扩展组件
    modelHeader: '',
    modelBody: '',
    modelFooter: ''
  },
  tableAction: '', //指定某张表的权限(这里填写表名,默认不用填写)
  buttons: { view: [], box: [], detail: [] }, //扩展的按钮
  TranslationTypeData:[],
  methods: {
     //下面这些方法可以保留也可以删除
    onInit() {  //框架初始化配置前，
        //示例：在按钮的最前面添加一个按钮
        //   this.buttons.unshift({  //也可以用push或者splice方法来修改buttons数组
        //     name: '按钮', //按钮名称
        //     icon: 'el-icon-document', //按钮图标vue2版本见iview文档icon，vue3版本见element ui文档icon(注意不是element puls文档)
        //     type: 'primary', //按钮样式vue2版本见iview文档button，vue3版本见element ui文档button
        //     onClick: function () {
        //       this.$Message.success('点击了按钮');
        //     }
        //   });

        //示例：设置修改新建、编辑弹出框字段标签的长度
        // this.boxOptions.labelWidth = 150;

      //在弹出框的第一个按钮后面动态添加一个按钮
        this.boxButtons.splice(0, 0,
            {
                name: "确认",
                icon: 'el-icon-scan',
                value: 'success',
                class: '',
                type: 'success',
                index: 0,//显示的位置
                onClick: function () {
                    if(this.editFormFields.OrderStatus==2){
                        this.editFormFields.OrderStatus=4
                    }
                    this.save();

                }
            });



      this.boxButtons.splice(1, 0,
          {
            name: "退回",
            icon: 'el-icon-scan',
            value: 'Del',
            class: '',
            type: 'success',
            index: 0,//显示的位置
            onClick: function () {

              if(this.editFormFields.OrderStatus>3){
                this.$Message.error("当前状态不可退回，请确认")
                return
              }
                this.$confirm('确认要退回吗?', '警告', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning',
                    center: true
                }).then(() => {
                    this.editFormFields.OrderStatus = '3'
                    this.save();
                });
            }
          });
      this.boxButtons.splice(1, 0,
          {
            name: "分配",
            icon: 'el-icon-scan',
            value: 'Save',
            class: '',
            type: 'success',
            index: 0,//显示的位置
            onClick: function () {
              if(this.editFormFields.OrderStatus!=4&&this.editFormFields.OrderStatus!=5){
                this.$Message.error("当前状态不可分配，请确认")
                return
              }
              this.$refs.gridHeader.open(this.editFormFields);
            }
          });
      //this.boxButtons[0].name='确认'


      //生成查询参数
      let params = {
        page: 1,
        rows: 10000
      };



      this.http.post("/api/Sys_TranslationType/GetPageDataAllow", params, true).then((y) => {

        if (y.rows.length > 0) {
          this.TranslationTypeData = y.rows


          console.log("datadatadata",this.TranslationTypeData)
        }
      });

      //设置明细表合计
      this.detailOptions.summary = true;
      console.log("detailOptions",this.detailOptions.buttons)


      this.detailOptions.buttons.splice(0, 4)

      //this.buttons.splice(1, 1)
    },
    onInited() {
      //框架初始化配置后
      //如果要配置明细表,在此方法操作
      //this.detailOptions.columns.forEach(column=>{ });

      this.detailOptions.columns.forEach(x => {
        if (x.field == 'EstimatedCost') {
          x.summary = true;
        }




      })


    },
    searchBefore(param) {
      //界面查询前,可以给param.wheres添加查询参数
      //返回false，则不会执行查询
      param.wheres.push({name:'OrderStatus',value:"2,3,4,5,6,7",DisplayType:"in"})
      return true;
    },
    searchAfter(result) {
      //查询后，result返回的查询数据,可以在显示到表格前处理表格的值
      return true;
    },
    addBefore(formData) {
      //新建保存前formData为对象，包括明细表，可以给给表单设置值，自己输出看formData的值
      return true;
    },
    updateBefore(formData) {
      //编辑保存前formData为对象，包括明细表、删除行的Id

      return true;
    },
    rowClick({ row, column, event }) {
      //查询界面点击行事件
      // this.$refs.table.$refs.table.toggleRowSelection(row); //单击行时选中当前行;
    },
    modelOpenAfter(row) {
        console.log("rowrowrowrow",row)
        if(row.OrderStatus==2||row.OrderStatus==4){
            this.boxButtons[0].disabled=false
            this.boxButtons[1].disabled=false
            this.boxButtons[2].disabled=false
        }else{
            this.boxButtons[0].disabled=true
            this.boxButtons[1].disabled=true
            this.boxButtons[2].disabled=true
        }

        if(row.OrderStatus>=3){
            this.boxButtons[2].disabled=true
        }else{
            this.boxButtons[2].disabled=false
        }

        if(row.OrderStatus==5){
            this.boxButtons[1].disabled=false
        }
        console.log("rowrowrowrow",this.boxButtons)
      //点击编辑、新建按钮弹出框后，可以在此处写逻辑，如，从后台获取数据
      //(1)判断是编辑还是新建操作： this.currentAction=='Add';
      //(2)给弹出框设置默认值
      //(3)this.editFormFields.字段='xxx';
      //如果需要给下拉框设置默认值，请遍历this.editFormOptions找到字段配置对应data属性的key值
      //看不懂就把输出看：console.log(this.editFormOptions)
    }
  }
};
export default extension;
