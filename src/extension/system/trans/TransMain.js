/*****************************************************************************************
 **  Author:jxx 2022
 **  QQ:283591387
 **完整文档见：http://v2.volcore.xyz/document/api 【代码生成页面ViewGrid】
 **常用示例见：http://v2.volcore.xyz/document/vueDev
 **后台操作见：http://v2.volcore.xyz/document/netCoreDev
 *****************************************************************************************/
//此js文件是用来自定义扩展业务代码，可以扩展一些自定义页面或者重新配置生成的代码

let extension = {
    components: {
        //查询界面扩展组件
        gridHeader: '',
        gridBody: '',
        gridFooter: '',
        //新建、编辑弹出框扩展组件
        modelHeader: '',
        modelBody: '',
        modelFooter: ''
    },
    tableAction: '', //指定某张表的权限(这里填写表名,默认不用填写)
    buttons: {
        view: [], box: [{
            disabled:false,
            name: "提交",
            icon: 'el-icon-scan',
            value: 'Edit',
            class: '',
            type: 'success',
            index: 0,//显示的位置
            onClick: function () {
                if (this.currentAction == 'Add') {
                    this.editFormFields.OrderStatus = '2'

                }
                this.editFormFields.OrderStatus = '2'
                this.save();
            }
        }], detail: []
    }, //扩展的按钮
    TranslationTypeData: [],
    LanguageData: [],
    methods: {
        //下面这些方法可以保留也可以删除
        onInit() {  //框架初始化配置前，
            //示例：在按钮的最前面添加一个按钮
            //   this.buttons.unshift({  //也可以用push或者splice方法来修改buttons数组
            //     name: '按钮', //按钮名称
            //     icon: 'el-icon-document', //按钮图标vue2版本见iview文档icon，vue3版本见element ui文档icon(注意不是element puls文档)
            //     type: 'primary', //按钮样式vue2版本见iview文档button，vue3版本见element ui文档button
            //     onClick: function () {
            //       this.$Message.success('点击了按钮');
            //     }
            //   });

            //示例：设置修改新建、编辑弹出框字段标签的长度
            // this.boxOptions.labelWidth = 150;

            //生成查询参数
            let params = {
                page: 1,
                rows: 10000
            };


            this.http.post("/api/Sys_TranslationType/GetPageDataAllow", params, true).then((y) => {

                if (y.rows.length > 0) {
                    this.TranslationTypeData = y.rows


                }
            });


            this.http.post("/api/Sys_Language/GetPageDataAllow", params, true).then((y) => {

                if (y.rows.length > 0) {
                    this.LanguageData = y.rows


                }
            });

            //设置明细表合计
            this.detailOptions.summary = true;

            this.detailOptions.buttons.splice(2, 2)



        },
        onInited() {
            //框架初始化配置后
            //如果要配置明细表,在此方法操作
            //this.detailOptions.columns.forEach(column=>{ });

            //设置明细表求字和段，后台需要实现GetDetailSummary方法
            this.detailOptions.columns.forEach(x => {
                let change=false;
                /*if (x.field == 'TranslationType') {
                    x.onChange = (column, row, tableData) => {
                        change=true
                        let needCalculate = false;
                        console.log(JSON.stringify(column))
                        let num = 0;
                        let dd = this.TranslationTypeData.filter(item => item.Id == (column.TranslationType))
                        if (dd.length > 0) {
                            needCalculate = (dd[0].Calculate == 1 ? true : false)
                        }
                        //给当前列同时设置上值

                        //注意：如果字段的值是与其他字段相乘的结果,合计可能不会刷新,请取消下面的注释方法
                        //2021.09.25更新后才能使用

                        if (needCalculate) {

                            let dd = this.LanguageData.filter(item => item.Id == (this.editFormFields.TranslationLanguages))

                            if (dd.length > 0) {
                                num = dd[0].Cost
                            }


                        }
                        column.EstimatedCost=(column.Quantity || 0) * num;
                    }
                }*/
                if (x.field == 'Quantity') {
                    x.formatter = (row) => {
                        if(this.currentAction == 'Add'){
                            row.Quantity=1
                        }
                        return row.Quantity

                    }
                }


                if (x.field == 'EstimatedCost') {
                    x.summary = true;
                }

                if (x.field == 'EstimatedCost') {
                    //将eidt设置为null不开启编辑
                    //x.edit = null;


                    x.formatter = (row) => {
                        //this.updateDetailTableSummaryTotal();
                        //return row.EstimatedCost
                        if (this.editFormFields.OrderStatus > 3) {
                            //console.log("1111", this.editFormFields.OrderStatus)
                            //console.log("1111", row.EstimatedCost)
                            return row.EstimatedCost
                        } else {
                            let needCalculate = false;
                            let num = 0;
                            let dd = this.TranslationTypeData.filter(item => item.Id == (row.TranslationType))
                            if (dd.length > 0) {
                                needCalculate = (dd[0].Calculate == 1 ? true : false)
                            }
                            //给当前列同时设置上值

                            //注意：如果字段的值是与其他字段相乘的结果,合计可能不会刷新,请取消下面的注释方法
                            //2021.09.25更新后才能使用

                            if (needCalculate) {

                                let dd = this.LanguageData.filter(item => item.Id == (this.editFormFields.TranslationLanguages))

                                if (dd.length > 0) {
                                    num = dd[0].Cost
                                }


                            }
                            this.updateDetailTableSummaryTotal();
                            row.EstimatedCost = (row.Quantity || 0) * num;
                            console.log("qqqq", row.EstimatedCost)
                            //可以设置计算规则 ，如：
                            return (row.Quantity || 0) * num

                        }
                        /* if(this.editFormFields.OrderStatus > 3){
                             console.log("1111",this.editFormFields.OrderStatus)
                             console.log("1111",row.EstimatedCost)
                             return  row.EstimatedCost
                         }else{
                             this.updateDetailTableSummaryTotal();
                             row.EstimatedCost = (row.Quantity || 0) * num;
                             console.log("2222",this.editFormFields.OrderStatus)
                             //可以设置计算规则 ，如：
                             return (row.Quantity || 0) * num
                             //返回table显示的值
                         }*/
                    }
                }


            })

        },
        searchBefore(param) {
            //界面查询前,可以给param.wheres添加查询参数
            //返回false，则不会执行查询
            return true;
        },
        searchAfter(result) {
            //查询后，result返回的查询数据,可以在显示到表格前处理表格的值
            return true;
        },
        addBefore(formData) {
            console.log("addBefore", formData)
            if (formData.detailData.length < 1) {
                this.$Message.error("请添加详细内容")
                return false;
            }
            //新建保存前formData为对象，包括明细表，可以给给表单设置值，自己输出看formData的值
            return true;
        },
        updateBefore(formData) {
            console.log("updateBefore", formData)

            if (formData.mainData.OrderStatus == 7) {
                console.log("2222222222222","11111111111111")
                formData.mainData.OrderStatus = 6
                return true;
            }


            if (formData.mainData.OrderStatus > 3) {
                this.$Message.error("此订单状态不可编辑，请确认")
                return false;
            }

            if (formData.detailData.length < 1) {
                this.$Message.error("请添加详细内容")
                return false;
            }
            //编辑保存前formData为对象，包括明细表、删除行的Id
            return true;
        },
        delBefore(ids, rows) { //查询界面的表删除前 ids为删除的id数组,,rows删除的行
            let flog = true
            rows.forEach(x => {
                if (x.OrderStatus > 3) {
                    flog = false
                }
            });
            if (!flog) {
                this.$Message.error("有不可删除的订单状态，请确认")
            }
            return flog;
        },

        delDetailRow(rows) { //弹出框删除明细表的行数据(只是对table操作，并没有操作后台)

            console.log("editFormFields", this.editFormFields)

            if (this.editFormFields.OrderStatus > 3) {
                this.$Message.error("此订单状态不可删除，请确认")
                return false;
            }
            return true;
        },
        rowClick({row, column, event}) {
            //查询界面点击行事件
            // this.$refs.table.$refs.table.toggleRowSelection(row); //单击行时选中当前行;
        },
        modelOpenAfter(row) {

            if (this.currentAction == 'Add') {
                this.editFormFields.PrimitiveLanguage = 1
                this.editFormFields.TranslationLanguages = 2
                this.editFormFields.UrgentStatus = "1"
                this.editFormFields.OrderStatus = "1"
                this.editFormFields.TransType = "3"
                this.editFormFields.ManageDept = "92d2afca-a963-485f-b4c0-1dde9f71a728"
                this.editFormOptions[8][0].hidden = true
                this.boxButtons[0].disabled=false
                this.boxButtons[1].disabled=false
            } else {
                this.editFormOptions[8][0].hidden = false

                if(this.editFormFields.OrderStatus>3){
                    this.boxButtons[0].disabled=true
                    this.boxButtons[1].disabled=true
                }else{
                    this.boxButtons[0].disabled=false
                    this.boxButtons[1].disabled=false
                }

                if(this.editFormFields.OrderStatus == 7){
                    this.editFormFields.OrderStatus == 6
                    this.save()
                   /* console.log("111", this.boxButtons[1])
                    this.boxButtons[1].disabled=false
                    this.boxButtons[1].name="完成";*/
                }
            }
            console.log("qqq", row)
            //点击编辑、新建按钮弹出框后，可以在此处写逻辑，如，从后台获取数据
            //(1)判断是编辑还是新建操作： this.currentAction=='Add';
            //(2)给弹出框设置默认值
            //(3)this.editFormFields.字段='xxx';
            //如果需要给下拉框设置默认值，请遍历this.editFormOptions找到字段配置对应data属性的key值
            //看不懂就把输出看：console.log(this.editFormOptions)


        },
        updateAfter(result){
            this.refresh();
            return false;
        }
    }
};
export default extension;
